package fibonacci;

import io.reactivex.Observable;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class FibonacciExercises {

	public Observable<Integer> fibonacci(int n) {
		return Observable.create(e-> {
			int a=0;
			int b=1;
			int sum;
			e.onNext(a);
			e.onNext(b);
			for (int i=0;i<n-2;i++){
				sum=a+b;
				a=b;
				b=sum;
				e.onNext(sum);
			}
			e.onComplete();
		});
	}

}
