package person.favorites

import person.types.PersonWithAddress
import io.reactivex.Observable

internal class FavoritesRepository(private val personBackend: PersonBackend, private val favoritesDatabase: FavoritesDatabase) {


    fun loadFavorites(): Observable<List<PersonWithAddress>> {

        /**
         * Provide an observable that only emits a list of PersonWithAddress if they are marked as favorite ones.
         */

      return favoritesDatabase.favoriteContacts()
              .flatMap { favs ->
                  personBackend.loadAllPersons().flatMap {
                      persons_ad -> Observable.fromArray(
                          persons_ad.filter {
                              favs.contains(it.person.id)
                          })
                  }
              }
    }
}
